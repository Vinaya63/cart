import React from 'react';
import './CartItem.css';

const CartItem = ({ item, onRemove }) => {
  return (
    <div className="cart-item">
      <img
        src={item.image}
        alt={item.name}
        className={`product-image ${item.added ? 'small-image' : ''}`}
      />
      <div className="product-info">
        <h3>{item.name}</h3>
        <p>Price: ₹{item.price.toFixed(2)}</p>
        <p>Quantity: {item.quantity}</p>
        <button onClick={onRemove}>Remove from Cart</button>
      </div>
    </div>
  );
};

export default CartItem;