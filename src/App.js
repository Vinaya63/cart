import React, { useState } from 'react';
import Header from './Header';
import ProductList from './ProductList';

const App = () => {
  const [cartItems, setCartItems] = useState([]);

  const addToCart = (item) => {
    const index = cartItems.findIndex((cartItem) => cartItem.id === item.id);
    if (index === -1) {
      setCartItems([...cartItems, item]);
    } else {
      const newCartItems = [...cartItems];
      newCartItems[index].quantity += 1;
      setCartItems(newCartItems);
    }
  };

  const removeFromCart = (id) => {
    const newCartItems = cartItems.filter((item) => item.id !== id);
    setCartItems(newCartItems);
  };

  return (
    <div className="App">
      <Header cartItems={cartItems} removeFromCart={removeFromCart} />
      <div className="content">
        <ProductList addToCart={addToCart} />
      </div>
    </div>
  );
};

export default App;