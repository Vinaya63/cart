import React from 'react';
import CartItem from './CartItem';
// import './Cart.css';

const Cart = ({ cartItems, removeFromCart, show, onClose }) => {
  const cartItemsList = cartItems.map((item) => (
    <CartItem key={item.id} item={item} onRemove={() => removeFromCart(item.id)} />
  ));

  const totalCost = cartItems.reduce(
    (total, item) => total + item.quantity * item.price,
    0
  );

  console.log(`Cart items count: ${cartItemsList.length}`);

  return (
    <div className={`cart ${show ? 'show' : ''}`}>
      <div className="cart-content">
        <h2>Your Cart</h2>
        {cartItemsList.length === 0 && (
          <p>No products in cart</p>
        )}
        {cartItemsList.length > 0 && (
          <div>
            {cartItemsList}
            <div className="total">
              <span>Total: </span>
              <span>₹{totalCost.toFixed(2)}</span>
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Cart;