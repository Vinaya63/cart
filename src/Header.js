import React, { useState } from 'react';
import Cart from './Cart';
import CartIcon from './CartIcon';
import './Header.css';

const Header = ({ cartItems, removeFromCart }) => {
  const [showCart, setShowCart] = useState(false);

  const handleCartClick = () => {
    setShowCart(!showCart);
  };

  const itemCount = cartItems.reduce((total, item) => total + item.quantity, 0);

  return (
    <header className="header">
      <h1 className="title">Product List</h1>
      <div className="cart-container">
        <CartIcon itemCount={itemCount} onClick={handleCartClick} />
        {itemCount > 0 && (
          <Cart
            cartItems={cartItems}
            removeFromCart={removeFromCart}
            show={showCart}
          />
        )}
      </div>
    </header>
  );
};

export default Header;