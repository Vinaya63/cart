import React from 'react';
import './CartIcon.css';

const CartIcon = ({ itemCount, onClick }) => {
  return (
    <div className="cart-icon" onClick={onClick}>
      <i className="fa fa-shopping-cart fa-lg"></i>
      <span className="cart-name">Cart</span>
      {itemCount > 0 && <span className="item-count">{itemCount}</span>}
    </div>
  );
};

export default CartIcon;