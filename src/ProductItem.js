import React from 'react';

const ProductItem = ({ product, addToCart }) => {
  const { id, name, images, price } = product;

  const handleAddToCart = () => {
    addToCart({ id, name, price, image: images[0], quantity: 1 });
  };

  return (
    <div className="product-item">
      <img src={images[0]} alt={name} />
      <h3>{name}</h3>
      <p>₹{price}</p>
      <button onClick={handleAddToCart}>Add to Cart</button>
    </div>
  );
};

export default ProductItem;